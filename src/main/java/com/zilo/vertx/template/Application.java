package com.zilo.vertx.template;

import com.zilo.vertx.core.annotations.EnableHttpConnectors;
import com.zilo.vertx.core.launcher.Launcher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@EnableHttpConnectors(HttpController = HttpController.class)
public class Application {
    private static final Logger log = LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        log.info("Starting...");
        // we need to set up this variables to use the correct logger and not JUL
        // all the configuration for this logger is under [src/resources/log4j2.yml]
        System.setProperty("vertx.logger-delegate-factory-class-name",
            "io.vertx.core.logging.Log4j2LogDelegateFactory");
        System.setProperty("log4j2.contextSelector",
            "org.apache.logging.log4j.core.async.AsyncLoggerContextSelector");

        // Start whole application. Please remember, it will create
        // normal threads (not daemon), so you need to stop your application explicitly
        Launcher.start(Application.class);
    }

    public static void init() { }
}
