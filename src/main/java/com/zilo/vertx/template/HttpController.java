package com.zilo.vertx.template;

import com.zilo.vertx.core.annotations.AutoInject;
import com.zilo.vertx.core.annotations.Endpoint;
import com.zilo.vertx.core.constants.HttpConstants;
import com.zilo.vertx.core.dto.Controllers;
import com.zilo.vertx.core.httpconnector.impl.HttpClient;
import com.zilo.vertx.core.httpconnector.impl.HttpServer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Data
@NoArgsConstructor
public class HttpController implements Controllers {

    @AutoInject
    private HttpServer httpServer;

    @AutoInject
    private HttpClient webClient;

    private static final Logger log = LogManager.getLogger(HttpController.class);

    public void init() {
        log.debug("Init Method for HttpController has been invoked");
    }

    @Endpoint(RequestMethod = HttpMethod.GET, uri = "/home")
    public void home(RoutingContext routingContext) {
        log.debug("Auth Page");

        HttpServerResponse response = routingContext.response();
        JsonObject data = new JsonObject(routingContext.user().principal().toString());
        response.setStatusCode(200).end(data.toString());
    }

    @Endpoint(RequestMethod = HttpMethod.GET, uri = "/api/v1")
    public void root(RoutingContext routingContext) {
        log.info("Root Page");
        HttpServerResponse response = routingContext.response();
        response.setStatusCode(200).end(HttpConstants.HTTP200);
    }

    @Endpoint(RequestMethod = HttpMethod.GET, uri = "/start")
    public void start(RoutingContext routingContext) {
        log.info("Start Page");
        HttpServerResponse response = routingContext.response();
        response.setStatusCode(200).end(HttpConstants.HTTP200);
    }
}
